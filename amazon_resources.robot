*** Settings ***
Library                SeleniumLibrary

*** Variables ***
${BROWSE}              chrome
${URL}                 https://www.amazon.com.br/
${MENU_ELETRONICOS}    //a[@href='/Eletronicos-e-Tecnologia/b/?ie=UTF8&node=16209062011&ref_=nav_cs_electronics'][contains(.,'Eletrônicos')]

*** Keywords ***
Abrir o navegador
    Open Browser        browser=${BROWSE}     
# Se desejar deixar o browser aberto adicone
# options=add_experimental_option("detach", True)
    Set Selenium Implicit Wait      10
    Set Window Size                 1920    1080

Fechar o navegador
    Capture Page Screenshot
    Close Browser
# 1° Cenario
Acessar a home page do site Amazon.com.br
    Go To          url=${URL}
    Wait Until Element Is Visible    locator=${MENU_ELETRONICOS}
Entrar no menu "Eletrônicos"
    Click Element                    locator=${MENU_ELETRONICOS}
Verificar se aparece a frase "${FRASE}"
    Wait Until Page Contains         text=${FRASE}
    Wait Until Element Is Visible    locator=//h2[contains(.,'Até 20% off em Eletrônicos e TVs | Black Friday')]
Verificar se o título da página fica "${TITULO}"    
    Title Should Be                   title=${TITULO}
Verificar se aparece a categoria "${NOME_CATEGORIA}"    
    Element Should Be Visible         locator=(//a[contains(.,'Computadores e Informática')])[2]

# 2° Cenario    
Digitar o nome de produto "${PRODUTO}" no campo de pesquisa
    Input Text                        locator=twotabsearchtextbox        text=${PRODUTO}       
Clicar no botão de pesquisa
    Click Button                      locator=nav-search-submit-button
Verificar o resultado da pesquisa se está listando o produto "${PRODUTO}"        
    Wait Until Element Is Visible    locator=//img[contains(@alt,'${PRODUTO}')]

# GHERKIN STEPS
# 1° Cenario
Dado que estou na home page da Amazon.com.br
    # uma keyword chamando outra
    Acessar a home page do site Amazon.com.br
    Verificar se aparece a frase "Até 20% off em Eletrônicos e TVs | Black Friday"

Quando acessar o menu "Eletrônicos"
    Entrar no menu "Eletrônicos"

Então o título da página deve ficar "Até 20% off em Eletrônicos e TVs | Black Friday"
    Verificar se o título da página fica "Eletrônicos e Tecnologia | Amazon.com.br"

E o texto "Eletrônicos e Tecnologia" deve ser exibido na página
    Verificar se aparece a frase "Até 20% off em Eletrônicos e TVs | Black Friday"

E a categoria "Computadores e Informática" deve ser exibida na página
    Verificar se aparece a categoria "Computadores e Informática"

# 2° Cenario
Quando pesquisar pelo produto "Xbox Series S"
    Digitar o nome de produto "Xbox Series S" no campo de pesquisa
    Clicar no botão de pesquisa

Então o título da página deve ficar "Amazon.com.br: Xbox Series S"
    Verificar se o título da página fica "Amazon.com.br: Xbox Series S"    

E um produto da linha "Xbox Series S" deve ser mostrado na página
    Verificar o resultado da pesquisa se está listando o produto "Console Xbox Series S"

Adicionar o produto "Console Xbox Series S" no carrinho
    Click Element                  locator=(//span[contains(.,'Xbox Series S')])[5]
    Click Button                   locator=//input[@title='Adicionar ao carrinho']

Verificar se o produto "Console Xbox Series S" foi adicionado com sucesso
    Wait Until Element Is Visible      locator=//a[contains(@id,'nav-cart')]
    Click Element                      locator=//a[contains(@id,'nav-cart')]
    Wait Until Element Is Visible      locator=(//span[contains(.,'Console Xbox Series S')])[9]

Remover o produto "Console Xbox Series S" do carrinho
    Wait Until Element Is Visible      locator=//input[@value='Excluir']    
    Click Element                      locator=//input[@value='Excluir']    

Verificar se o carrinho fica vazio
    Wait Until Element Is Visible      locator=(//span[contains(@class,'a-size-base')])[4]