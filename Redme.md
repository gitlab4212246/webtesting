# Executar todos os cenários
robot amazon_testes.robot

# Executar por título de CT
robot -t "Caso de Teste 02 - Pesquisa de um Produto" amazon_testes.robot    

# Documentação Robot
robot --help

# Executar por Tag
robot -i menus amazon_testes.robot    


# Executar excluindo esta Tag
robot -e menus amazon_testes.robot    


# Executar colocando logs em uma pasta
robot -d resultados amazon_testes.robot    

# Robot substituir o valor de uma variável global ${BROWSE}
robot -v BROWSE:firefox resultados amazon_testes.robot  

robot -v BROWSE:firefox -i menus -d resultados amazon_testes.robot  

# Executar com o bdd
robot -d resultados amazon_testes_geherking_bdd.robot    

